/*
 * mcontrol.c
 *
 *  Created on: 15.06.2018
 *      Author: alex
 */

#include "stm32f4xx.h"
#include "mcontrol.h"
#include "util.h"

__IO uint32_t *tim_ccr = &TIM1->CCR1;

void forward() {
	TIM1->CCR1 = *tim_ccr;
	tim_ccr = &TIM1->CCR1;
	M_OUT(GPIOA, 9);
	GPIOA->MODER = 0xa8040000;
	GPIOA->BSRRH = 1 << 9;
	M_OUT(GPIOB, 14);
	GPIOB->BSRRL = 1 << 14;

	M_ALT(GPIOA, 8);
	M_ALT(GPIOB, 13);

}

void backward() {
	TIM1->CCR2 = *tim_ccr;
	tim_ccr = &TIM1->CCR2;
	M_OUT(GPIOA, 8);
	GPIOA->BSRRH = 1 << 8;
	M_OUT(GPIOB, 13);
	GPIOB->BSRRL = 1 << 13;

	M_ALT(GPIOA, 9);
	M_ALT(GPIOB, 14);
}

void brake() {
	M_OUT(GPIOA, 8);
	M_OUT(GPIOA, 9);
	GPIOA->BSRRH = 1 << 8 | 1 << 9;
	M_OUT(GPIOB, 13);
	M_OUT(GPIOB, 14);
	GPIOB->BSRRL = 1 << 13 | 1 << 14;
}
