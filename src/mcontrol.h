/*
 * mcontrol.h
 *
 *  Created on: 15.06.2018
 *      Author: alex
 */

#ifndef MCONTROL_H_
#define MCONTROL_H_

void forward();
void backward();
void brake();

#endif /* MCONTROL_H_ */
