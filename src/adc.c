/*
 * adc.c
 *
 *  Created on: 15.06.2018
 *      Author: alex
 */

#include "adc.h"

#include <stm32f4xx.h>
#include "mcontrol.h"

void init_adc() {
	RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;

	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
	GPIOA->MODER |= 3 << 2 * 1;
	//TESTING

	GPIOA->MODER |= 1 << 2 * 5;

	u32 cr2 = ADC1->CR2;
	cr2 |= ADC_CR2_CONT;
	ADC1->CR2 = cr2;

	ADC1->HTR = 3700;
	ADC1->LTR = 400;

	NVIC_EnableIRQ(ADC_IRQn);

	ADC1->SQR3 = 1;

	ADC1->CR2 |= ADC_CR2_ADON;
	ADC1->CR2 |= ADC_CR2_SWSTART;

	while (!(ADC1->SR & ADC_SR_EOC))
		;
	u32 cr1 = ADC1->CR1;
	cr1 |= ADC_CR1_AWDEN;
	cr1 |= ADC_CR1_AWDIE;
	ADC1->CR1 = cr1;

}

void ADC_IRQHandler() {
	if (ADC1->SR & ADC_SR_AWD) {
		GPIOA->BSRRL = 1 << 5;
//		brake();
		ADC1->SR &= ~ADC_SR_AWD;
	}
}

